//var express = require('express')
//  , app = express()
//  , http = require('http')
//  , server = http.createServer(app)
//  , io = require('socket.io').listen(server);
//server.listen(9000);

var express = require('express')
    , app = express()
    , http = require('http')
    , server = http.createServer(app)
    , io = require('socket.io').listen(server);
server.listen(3000);

io.configure('production',function(){
    io.set('transports',[
        'xhr-polling',
        'jsonp-polling',
        'flashsocket',
        'websocket',
        'htmlfile'
    ]);
});

console.log('Server running at localhost:3000');

//}).listen(3000, "127.0.0.1");


var _ = require('underscore');

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public/toolbar'));

// routing
app.get('/', function (req, res) {
//  res.sendfile(__dirname + '/index.html');
    res.sendfile(__dirname + '/public/toolbar/index.html');
});

// usernames which are currently connected to the chat
var usernames = {};
var Games = {};
var Players = {};
var lobby = 'lobby';
var defaultHostSymbol = 'X';
var sender = {server:'server'};

var e = {   updateChat:'updateChat',updatePrivateChat:'updatePrivateChat',joinUser:'joinUser',
            updateUser:'updateUser',updateUsers:'updateUsers',
            updateGames:'updateGames',updateGame:'updateGame', gameIsCreated:'gameIsCreated',opponentJoin:'opponentJoin',
            joinGame:'joinGame',leaveGame:'leaveGame',leaveGameSuccess:'leaveGameSuccess',
            updateGameState:'updateGameState',confirmRematch:'confirmRematch',updatePlayersScore:'updatePlayersScore',
            gameIsRestarted:'gameIsRestarted',

        };

io.sockets.on('connection', function (socket) {

    socket.on('sendChat', function (data) {
        broadcastToThisRoom(socket.room,'updateChat',{error:0, sender:socket.username,msg:data.message,data:null})
    });
    socket.on('sendPrivateChat', function (data) {
        broadcastToThisRoom(socket.room,'updatePrivateChat',{error:0, sender:socket.username,msg:data.message,data:null})
    });

    socket.on('sendPrivateChat',function(data){
        if(!_.isUndefined(data.gameId)){
            var game = Games[data.gameId];
            if(!_.isUndefined(game)){
                // broadcast to all inside the Game Room
                io.sockets.in(data.gameId).emit('updatePrivateChat',{error:0, sender:socket.username,msg:data.message,data:null});
            }else{
                myLog('Error : sendPrivateChat : invalid gameId('+data.gameId+') is provided.');
            }
        }
    });

    socket.on('addUser', function(param){

        // Create new Player first so Player class will take care of giving name scheme and storing previous score if given
        // if user pass empty name or doesn't pass name at all, Player class will generate one
        var player = new Player(param);
        Players[player.id] = player;
        myLog(player.name+'('+player.id+')'+'is added with score ('+player.score+'). Total players : #'+ _.size(Players));

        // add the client's username to the global list
        usernames[player.name] = player.name;

        // we store the username in the socket session for this client
        socket.username = player.name;
        socket.pid = player.id;
        socket.room = lobby;

        //join to Lobby as default
        socket.join(socket.room);



        // echo to myself (newly created user)
        broadcastToMyself(e.updateChat,{error:0, sender:sender.server,msg:'you have joined',data:null});
        broadcastToMyself(e.updateUser, {error:0, sender:sender.server,data:player});
        broadcastToMyself(e.updateGames,{error:0, sender:sender.server, msg:'', data:passGameList(Games)});
        broadcastToMyself(e.updateUsers,{error:0, sender:sender.server, msg:'', data:Players});

        // update the list of users in chat, client-side
        broadcastToEveryone(e.joinUser, {error:0, sender:sender.server, msg:'', data:player});

        // echo globally (all clients) that a person has connected
        broadcastToLobbyExceptMe(e.updateChat, {error:0, sender:sender.server,msg:player.name+' has connected', data:null});


    });

    socket.on('gameCreate',function(player){

        // pass the Player object. Search from Players[] if none is provided
        var p = (_.isUndefined(player) || _.isUndefined(player.id)) ? Players[socket.pid] : Players[player.id];

        // set Symbol passed from Client
        if(p && !_.isUndefined(player.symbol)){
            p.symbol = player.symbol;
        }

        // check if player is already in Game or not
        if(_.isUndefined(p) || p.isPlaying === true){

            broadcastToMyself(e.gameIsCreated,{error:1, sender:sender.server, msg:'gameCreate request failed. You are already in a game.', data:null});

        }else{

            p.isPlaying = true;
            p.score = 0;    // reset score

            // create new game
            var game = new Game(p.id);
            game.gameReady();

            // store in Global List
            Games[game.id] = game;
            myLog('new game created : '+game.id+' - total Games - '+ _.size(Games));

            // join Game Room
            socket.room = game.id;
            joinRoom(socket,socket.room);

            // echo to client(host) connected
            broadcastToMyself(e.gameIsCreated,{error:0, sender:sender.server,msg:'', data:passGameData(game)});

            // echo new Game to Lobby
            broadcastToLobby(e.updateGame,{error:0, sender:sender.server, msg:'', action:'add', data:passGameData(game)});

            // echo GameList globally (all clients) that a person status has connected
//            socket.broadcast.emit('updateUsers', {error:0, sender:sender.server, msg:'', data:Players});

            myLog('gameCreate : ');
            myLog(p);
        }

    });

    socket.on('joinGame',function(param){
        /*
         * param => {gameId, playerId}
         *
         * Objective:
         * - find Game with given gameId
         * - find Player with given playerId
         * - if both of them are success,
         *      set Game State to `ongoing`,
         *      set game.opponent to playerId
         * - broadcast to both Host & Opponent about the game is started.
         * - pass {gameId, gameState, currentPlayer}
         */
        var success = false;

        if(!_.isUndefined(param.gameId)){
            var game = Games[param.gameId];
            if(!_.isUndefined(game) && game.gameStatus === game.enumStatus.start){

                var p = (_.isUndefined(param.playerId)) ? Players[socket.pid] : Players[param.playerId];

                // pass the Player object. Search from Players[] if none is provided
                if(!_.isUndefined(p)){

                    p.symbol = getOppositeSymbol(getPlayerById(game.host).symbol); // set default Symbol opposite to Host
                    p.isPlaying = true;
                    p.score = 0;

                    // join the Game Room
                    joinRoom(socket,game.id);

                    // start the game
                    game.opponent = p.id;
                    game.gameStart();

                    var h = getPlayerById(game.host);
                    var hostSymbol = defaultHostSymbol;
                    if(!_.isUndefined(h) && !_.isUndefined(h.symbol))
                        hostSymbol = h.symbol;

                    // broadcast to all inside the Game Room
                    broadcastToThisRoom(game.id, e.opponentJoin,{error:0, sender:sender.server,msg:'',data:passGameData(game),special:{hostSymbol:hostSymbol ,opponentSymbol:p.symbol},players:{host:getPlayerById(game.host),opponent:getPlayerById(game.opponent)}});
                    broadcastToThisRoom(game.id, e.updatePrivateChat,{error:0, sender:sender.server,msg: p.name+' joined the game',data:null});

                    // broadcast to Lobby about this Game status
                    broadcastToLobby(e.updateGame,{error:0, sender:sender.server, msg:'', action:'update', data:passGameData(game)});

                    // echo GameList globally (all clients) that a person status has connected
//                        io.sockets.in(socket.room).emit('updateUsers', {error:0, sender:sender.server, msg:'', data:Players});
//                    socket.broadcast.emit('updateUsers', {error:0, sender:sender.server, msg:'', data:Players});

                    success = true;

                }
            }
        }

        if(success){
            myLog('joinGame : '+game.id+' | opponentPlayer : '+ p.name+' ('+ p.id+')');

            var param = {error:0, sender:sender.server,msg:'', data:_.pick(game,'id','gameState','currentPlayer','gameStatus')};

            // echo to client(host) connected
            socket.emit('joinGame', param);

            // echo globally (all clients) that a person has connected
//            socket.broadcast.emit('gameIsCreated',param);

        }else{
            var param = {error:1, sender:sender.server,msg:'Invalid GameID or PlayerID', data:null};
            // ONLY echo to client(host) connected
            socket.emit('joinGame', param);
        }
    })

    socket.on('leaveGame',function(param){
        /*
         * param => {gameId, playerId}
         *
         * Objective:
         * - change Game status and record Foul Player
         * - broadcast to the room about the game is incomplete along with foul player
         * - change foul player's player.isPlaying = false
         * - broadcast to self about status changes
         */

        if(_.isUndefined(param) || _.isUndefined(param.gameId) || _.isUndefined(param.playerId)){

            broadcastToMyself(e.leaveGameSuccess,{error:1, sender:sender.server,msg:'gameId or playerId is empty', data:null});
            return;
        }

        leaveRoom(socket,param.gameId);

        var p = (_.isUndefined(param.playerId)) ? Players[socket.pid] : Players[param.playerId];

        var game = !_.isUndefined(param.gameId) ? Games[param.gameId] : undefined;
        if(!_.isUndefined(game)){

            game.gameLeave(p.id);

            // broadcast to everyone in the room except me coz I'm leaving anyway. I don't care what happen in here
            broadcastToThisRoomExceptMe(game.id, e.leaveGame, {error:0, sender:sender.server, msg:'', data:passGameData(game)});

            // chat log
            broadcastToThisRoomExceptMe(game.id, e.updateChat, {error:0, sender:sender.server,msg:p.name+' has left the game.', data:null});

            // server must send Game List update to all users in lobby
            broadcastToLobby(e.updateGame,{error:0, sender:sender.server, msg:'', action:'delete', data:passGameData(game)});

            // delete Game from record
            deleteGame(game.id);

        }

        /*
         * Broadcast to User later coz user may have advantage after the Game is destroyed from record so `updateGames` will not have old data.
         * Otherwise, `updateGames` will still have old game.
         */
        if(!_.isUndefined(p)){

            p.isPlaying = false;
        }

        // broadcast to self
        broadcastToMyself(e.leaveGameSuccess,{error:0, sender:sender.server, data:getPlayerById(param.playerId)});
        broadcastToMyself(e.updateGames,{error:0, sender:sender.server, msg:'', data:passGameList(Games)});

    })


    socket.on('gameMove',function(param){
        /*
         * param => {gameId, playerId, move}
         */
        var game = Games[param.gameId];
        var pass = false;

        if(!_.isUndefined(game) && game.gameStatus === game.enumStatus.ongoing){

            if(game.gameMove(param.move) === false || game.currentPlayer !== socket.pid){

                var returnObj = {error:1, sender:sender.server, msg:'Invalid move', data:null};

                broadcastToMyself(e.updateGameState,returnObj);

            }else{

                var result = game.checkGameState();  // check game Win/Lose/Draw condition

                if(_.isBoolean(result) && result === true){

                    // draw
                    game.updatePlayerState();

                    var winner = getPlayerById(game.winner);
                    // broadcast to all inside the Game Room
                    broadcastToThisRoom(game.id, e.updateGameState,{error:0, sender:sender.server,msg:'',data:passGameData(game),players:{host:getPlayerById(game.host),opponent:getPlayerById(game.opponent)}});
                    broadcastToThisRoom(game.id, e.updateChat, {error:0, sender:sender.server,msg:'This game is draw.', data:null})
                    broadcastToThisRoom(game.id, e.updatePlayersScore,{error:0, sender:sender.server,msg:'',data:null,players:{host:getPlayerById(game.host),opponent:getPlayerById(game.opponent)}});

                }else if(_.isArray(result)){

                    // winner result
                    game.updatePlayerState();

                    var winner = getPlayerById(game.winner);
                    winner.score = winner.score+1;

                    // broadcast to all inside the Game Room
                    broadcastToThisRoom(game.id, e.updateGameState,{error:0, sender:sender.server,msg:'',data:passGameData(game),result:result,players:{host:getPlayerById(game.host),opponent:getPlayerById(game.opponent)}});
                    broadcastToThisRoom(game.id, e.updateChat, {error:0, sender:sender.server,msg:winner.name+' won the game.', data:null})
                    broadcastToThisRoom(game.id, e.updatePlayersScore,{error:0, sender:sender.server,msg:'',data:null,players:{host:getPlayerById(game.host),opponent:getPlayerById(game.opponent)}});

                }else{
                    // still ongoing. game move left is `result`

                    // change Turn
                    game.changeTurn();

                    var returnObj = {error:0, sender:sender.server, msg:'Success', data:passGameData(game)};

                    broadcastToThisRoom(game.id, e.updateGameState,{error:0, sender:sender.server,msg:'',data:passGameData(game)});

                    myLog("Successfully moved. Game Id : "+game.id+". Game State : "+
                        game.gameState[0]+
                        game.gameState[1]+
                        game.gameState[2]+
                        game.gameState[3]+
                        game.gameState[4]+
                        game.gameState[5]+
                        game.gameState[6]+
                        game.gameState[7]+
                        game.gameState[8]);
                }
            }

        }else{
            // invalid
            myLog("GameId : "+game+" or Game Status : "+game.gameStatus+" is not valid.");

        }
    });

    socket.on('requestGameRestart',function(param){
        /*
         * param => {gameId}
         *
         * Objective:
         * - check if Game is valid and Status is 'end'
         * - if success,
         *      set Game State to `ongoing`,
         * - broadcast to both Host & Opponent about the game is started.
         * - pass {gameId, gameState, currentPlayer}
         */
        if(_.isUndefined(param)){
            // echo to client(host) connected
            broadcastToMyself(e.confirmRematch,{error:1, sender:sender.server,msg:'gameId is empty', data:null});
        }

        if(!_.isUndefined(param.gameId)){
            var game = Games[param.gameId];
            if(!_.isUndefined(game) && (game.gameStatus !== game.enumStatus.ongoing)){

                var player = getPlayerById(socket.pid);
                var msg = !_.isUndefined(player) ? player.name+' has challenged for Rematch.' : 'Rematch is requested.';

                var to = (game.host === socket.pid) ? game.opponent : game.host;
                var returnObj = {error:0, sender:sender.server, msg:'', data:passGameData(game), to:to};

                broadcastToThisRoom(game.id, e.confirmRematch,returnObj);

                broadcastToThisRoom(game.id, e.updateChat, {error:0, sender:sender.server,msg:msg})


            }else{

                broadcastToMyself(e.confirmRematch,{error:1, sender:sender.server,msg:'Fail to restart the game', data:null});
            }
        }

    });

    socket.on('confirmRematch',function(param){
        /*
         * param => {gameId,rematch}
         *
         * Objective:
         * - check if Game is valid and Status is 'end'
         * - if success,
         *      set Game State to `ongoing`,
         * - broadcast to both Host & Opponent about the game is started.
         * - pass {gameId, gameState, currentPlayer}
         */

        if(_.isUndefined(param.gameId)){
            // echo to client(host) connected
            broadcastToMyself(e.gameIsRestarted,{error:1, sender:sender.server,msg:'gameId is empty'});

            return;
        }

        var game = Games[param.gameId];

        if(!_.isUndefined(game) && game.gameStatus !== game.enumStatus.ongoing){

            if(param.rematch === true){

                game.gameRestart();

                var player = getPlayerById(socket.pid);
                var msg = !_.isUndefined(player) ? player.name+' has accepted the challenge.' : 'Challenge is accepted.';

                var returnObj = {error:0, sender:sender.server, msg:'', data:passGameData(game),players:{host:getPlayerById(game.host),opponent:getPlayerById(game.opponent)}};
                broadcastToThisRoom(game.id, e.gameIsRestarted,returnObj);

                broadcastToThisRoom(game.id, e.updateChat, {error:0, sender:sender.server,msg:msg})

            }else{

                var player = getPlayerById(socket.pid);
                var msg = !_.isUndefined(player) ? player.name+' has rejected the challenge.' : 'Challenge is rejected';
                var to = (game.host === socket.pid) ? game.opponent : game.host;

                var returnObj = {error:1, sender:sender.server, msg:'', data:passGameData(game),to:to};
                broadcastToThisRoom(game.id, e.gameIsRestarted,returnObj);

                broadcastToThisRoom(game.id, e.updateChat, {error:0, sender:sender.server,msg:msg})
            }


        }else{

            broadcastToMyself(e.gameIsRestarted,{error:1, sender:sender.server,msg:'Fail to restart the game', data:null});

        }

    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function(){

        // remove the username from global usernames list

        deleteUsername(socket.username);
        deletePlayer(socket.pid);

        // remove the Game when the host is disconnected
        var g = _.find(Games, function(value,key){ return value.host === socket.pid;});
        !_.isUndefined(g) ? deleteGame(g.id) : false ;

        myLog(socket.username+' has left. Total players left : #'+ _.size(Players));

        if(_.isUndefined(socket.username))
            return;

        // Server will broadcast only if this user is valid


        // update list of users in chat, client-side
//        io.sockets.in(socket.room).emit('updateUsers', {error:0, sender:sender.server, msg:'', data:Players});
        socket.broadcast.emit('updateUsers', {error:0, sender:sender.server, msg:'', data:Players});

        // echo to current room that this client has left
        // if socket.room is undefined, just use "lobby"
        var room = _.isUndefined(socket.room) ? lobby : socket.room;
        io.sockets.in(room).emit('updateChat',{error:0, sender:sender.server,msg:socket.username + ' has left', data:null});

//        io.sockets.emit('updateGames',{error:0, sender:sender.server, msg:'', data:passGameList(Games)});
        if(!_.isUndefined(g) && !_.isUndefined(g.id))
            io.sockets.in(socket.room).emit('updateGame',{error:0, sender:sender.server, msg:'', action:'delete', data:{id:g.id}});

    });

    function broadcastToMyself(event,param){
        socket.emit(event, param);
    }

    function broadcastToThisRoomExceptMe(room,event,param){
        socket.broadcast.to(room).emit(event, param);
        // to broadcast information to a certain room (excluding the client):
    }

    function broadcastToThisRoom(room,event,param){
        io.sockets.in(room).emit(event, param);
    }

    function broadcastToLobbyExceptMe(event,param){
        socket.broadcast.to(lobby).emit(event,param);
    }

    function broadcastToLobby(event,param){
        io.sockets.in(lobby).emit(event, param);
    }

    function broadcastToEveryone(event,param){
        io.sockets.emit(event,param);
    }

    function passGameData(game){
        return _.pick(game,'id','gameState','host','currentPlayer','gameStatus','opponent','winner');
    }

    function passGameList(gameList){
        var picked = [];
        _.each(gameList, function(value, key){
            if(value.gameStatus == value.enumStatus.start)  {
                picked.push(_.pick(value,'id','host','gameStatus'));
                myLog('passGameList : id'+value.id+' | host : '+value.host);
            }

        });
        return picked;
    }

});

/*
 *
 * Game Class
 *
 */

function Game(hostPlayer){

    /*
     [ O         X       O]
     [ X         O       O]
     [ X         X       O]
     */

    this.id = getUniqueId('game_');
    this.gameState = ['-','-','-','-','-','-','-','-','-'];
    this.host = hostPlayer; // playerId
    this.players = [];      // [player1, player2]   //TODO: may remove in future
    this.opponent = null;   // playerId
    this.currentPlayer = null;   // playerId
    this.winner = null;        // playerId

    this.enumStatus = {start:'start',ongoing:'ongoing',end:'end',incomplete:'incomplete'};
    this.foul = null;

    this.gameStatus = this.enumStatus.start;    // default when game is created

    this.gameReady= function(){
        this.gameStatus = this.enumStatus.start;
        this.currentPlayer = this.host;
    }

    this.gameStart = function(){
        this.gameStatus = this.enumStatus.ongoing;
    }

    this.gameRestart = function(){
        this.gameStart();
        this.gameState = ['-','-','-','-','-','-','-','-','-'];
        this.currentPlayer = this.winner === this.host ? this.opponent : this.host;
        this.winner = null;
        this.foul = null;
    }

    this.gameEnd = function(){
        this.gameStatus = this.enumStatus.end;
    }

    this.gameLeave = function(pid){
        this.gameStatus = this.enumStatus.incomplete;
        this.foul = pid;
    }

    this.gameMove = function(move){
        if(this.gameState[move] === '-'){

            // update to GameState
            this.gameState[move] = getPlayerById(this.currentPlayer).symbol;
            return move;
        }else{
//            return {error:1,msg:'Invalid Move'};
            return false;
        }
    }

    this.changeTurn = function(){
        this.currentPlayer = this.currentPlayer === this.host ? this.opponent : this.host;
    }

    this.gameOver = function(){
        this.winner = this.currentPlayer;
        this.gameStatus = this.enumStatus.end;
    }

    this.gameDraw = function(){
        this.winner = null;
        this.gameStatus = this.enumStatus.end;
    }

    this.checkGameState = function(){

        // winning state. ordering doesn't matter. as long as 3 numbers are matched, GameOver.
        var winning = [[0,1,2], [0,3,6], [0,4,8], [1,4,7], [2,5,8], [2,4,6], [3,4,5], [6,7,8]];
        var currentPlayerMoves = [];
        var gameOver = [];

        // collect last player moves
        var index = 0;
        for(var symbol in this.gameState){
            if(this.gameState[symbol] === getPlayerById(this.currentPlayer).symbol)
                currentPlayerMoves.push(index);
            index = index + 1;
        }

        // loop and find the result
        var first,second,third;
        var stack1 = [];
        var stack2 = [];
        var stack3 = [];

        // if moves are less than 3, no need to continue
        if(currentPlayerMoves.length < 3)
            return false;

        // check winning move
        function checkWinnerV1(usermoves){
            var result;
            for(var index in winning){
                result = _.intersection(winning[index],usermoves);
                if(_.isArray(result) && result.length === 3){
                    console.log(winning[index]+' is winner');
                    gameOver.push(winning[index]);
                }
            }
        }

        checkWinnerV1(currentPlayerMoves);
        var moveLeft = calculateEmptyMove(this.gameState);

        if(_.isArray(gameOver) && gameOver.length > 0){   // GameOver, Winner's move

            this.gameOver();
            // return first,second,third values

            return gameOver;

        }else if(moveLeft>0){   // Continue the game

            return moveLeft;

        }else{  // Game is Draw

            this.gameDraw();
            return true;
        }

    }

    this.updatePlayerState = function(){
        var host = getPlayerById(this.host);
        if(!_.isUndefined(host)){
            host.isPlaying = false;
            if(this.winner === host.id)
                host.score = host.score + 1;
        }

        var opp = getPlayerById(this.opponent);
        if(!_.isUndefined(opp)){
            opp.isPlaying = false;
            if(this.winner === opp.id)
                opp.score = opp.score + 1;
        }

        if(this.winner === host.id)
            host.score = host.score + 1;
        if(this.winner === opp.id)
            opp.score = opp.score + 1;
    }

    function calculateEmptyMove(gameState){
        var leftMove = 0;
        for(var index in gameState){
            if(gameState[index] === '-')
                leftMove = leftMove+1;
        }
        return leftMove;
    }
}

function Player(param){

    var name = param.username;

    // public properties
    this.symbol = '';
    this.id = getUniqueId('player_');
    this.name = (name === "" || _.isUndefined(name) || _.isEmpty(name)) ? this.id : name;
    this.score = _.isUndefined(param.score) ? 0 : param.score;  // store the score if user can pass from Cookie.
    this.isPlaying = false;
}


function deleteGame(gameId){
    delete Games[gameId];
}

function deletePlayer(playerId){
    delete Players[playerId];
}

function deleteUsername(username){
    delete usernames[username];
}

function leaveRoom(socket,currentRoom){
    socket.leave(currentRoom);
    socket.join(lobby);
    socket.room = lobby;
}

function joinRoom(socket,newRoom){
    socket.leave(lobby);
    socket.join(newRoom);
    socket.room = newRoom;
}


function getPlayerById(id){
    return Players[id];
}

function myLog(msg){
    console.log(msg);
}

function getLinuxSeconds(){
    return (new Date().getTime() / 1000).toFixed();
}

function getUniqueId(prefix){
    return _.uniqueId(prefix);
}

function getOppositeSymbol(sym){
    return sym === 'X' ? 'O' : 'X';
}